### Hi there 👋
My name is Nadia. I am a Junior JavaScript developer and currently:
- learning React and React Native 📕 
- looking for an inspiring job 💣

Check out [my portfolio](https://nadiia-portfolio.netlify.app/) or recent projects:
- English for Kids:
   - repo https://github.com/magklax/english-for-kids/tree/english
   - deploy https://magklax-english-for-kids.netlify.app/
- Escape Room Quest:
    - repo https://github.com/magklax/rsclone
    - deploy https://rsclone-trap-quest.netlify.app/
- Adopt Pets React Native Mobile App:
    - repo https://github.com/magklax/my-pets-app
    - deploy https://expo.io/@magklax/my-pets-app (requires expo)


